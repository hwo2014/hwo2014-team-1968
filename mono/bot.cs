using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Bot {

	static string botName;
	JArray track = null;

	double currentAngle;
	double currentPos = 0;
	int currentPiece = 0;
	double currentPieceAngle;

	int currentLap = 0;
	int lastLap = 0;

	double lastTrackPosition = 0;
	double currentTrackPosition = 0;

	double trackLength = 0;
	static double friction = 0.9;
	double speed = 0;
	bool turboOn = false;
	JToken turbo = null;
	int crashes = 0;
	bool crashed = false;
	bool brake;

	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        botName = args[2];
        string botKey = args[3];

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			new Bot(reader, writer, new Join(botName, botKey));
		}
	}

	private StreamWriter writer;

	JToken findcar( JArray data, string id )
	{
		foreach (var n in data)
		{
			if ( n["id"]["name"].ToString() == id )
				return n;
			else
				Console.WriteLine("found " + n["id"]["name"].ToString() );
		}
		return null;
	}


	double getAngle(int piece)
	{
		if (track == null)
			return 0;
		var p = track[piece%(track.Count-1)];
		if ( p== null ) 
			return 0;
		var a = p["angle"];
		if (a == null)
			return 0;
		return double.Parse(a.ToString());
	}

	// [0,1]
	double getPosInPiece(int piece, double pos)
	{
		return pos / getLength(piece);
	}

	bool muchSpeed(int piece)
	{
		int m = 0;
		int p = piece - (currentPos > 0.8 ? 1 : 0) + track.Count;
		for (int a = 0; a < 2; a++)
			m = getAngle((p - a) % (track.Count - 1)) == 0 ? m + 1 : 0;
		bool beginning = currentLap == 0 && currentPiece < 2;
		return (m > 1 && speed>100  && !beginning);
	}

	double getLength(int piece)
	{
		var t = track[(piece + track.Count) % (track.Count - 1)];
		var l = t["length"];
		if ( l!=null ) return double.Parse(l.ToString());
		// not a straight line
		var ang = t["angle"];
		var rad = t["radius"];
		//TODO: something other
		if ( ang==null || rad == null ) 
			return 10;
		var a = double.Parse(ang.ToString());
		var r = double.Parse(rad.ToString());
		// is one curve piece 45 degrees or what? 
		return Math.Abs(a * Math.PI * r / 180.0);
	}


	bool isNextBad()
	{
		double next = getAngle(currentPiece+1);
		if ( next == 0 ) return false;
		if (next >= 45) return true;
		return false;
	}

	bool isTurboOk()
	{
		if (turbo == null || turboOn)
			return false;
		double m = 0;
		int p = currentPiece + (int)(currentPos + 0.2);
		for (int a = 0; a < 2; a++)
		{
			int piece = (p + a) % (track.Count - 1);
			m = getAngle(piece) == 0 ? m + getLength(piece) : 0;
		}
		if (m > 200) return true;
		if ( getAngle(currentPiece)==0 && currentPos < 0.4 && !isNextBad() )
			return true;
		return false;
	}

	double estimatedSpeed()
	{
		if (currentTrackPosition < lastTrackPosition)
			return speed;
		else
			return currentTrackPosition - lastTrackPosition;
	}

	Bot(StreamReader reader, StreamWriter writer, Join join) {
		this.writer = writer;
		string line;

		send(join);

		while((line = reader.ReadLine()) != null) {
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
			switch(msg.msgType) {
				case "yourCar":
					Console.WriteLine("your car");
					send(new Ping());
					break;
				case "carPositions":
					var me = findcar((JArray)msg.data, botName);

					double throttle = 1.0;

					int piece = int.Parse(me["piecePosition"]["pieceIndex"].ToString());
					currentPos = double.Parse(me["piecePosition"]["inPieceDistance"].ToString());
					if (piece != currentPiece)
						Console.Write("\n[" + piece + "] ");

					int add = (int)(getPosInPiece(piece, currentPos) + 0.1);
					double curr = getAngle(piece + add);
					double next = getAngle(piece + 1 + add);
					double currAbs = Math.Abs(getAngle(piece));
					double nextAbs = Math.Abs(getAngle(piece+1));
					currentAngle = double.Parse(me["angle"].ToString());

					lastLap = currentLap;
					int lastPiece = currentPiece;
					currentPiece = piece;
					currentPieceAngle = curr;
					
					lastTrackPosition = currentTrackPosition;
					currentLap = int.Parse(me["piecePosition"]["lap"].ToString());
					//currentTrackPosition = currentLap * trackLength + double.Parse(track[currentPiece]["lengthThisFar"].ToString()) + currentPos;
					currentTrackPosition = currentLap * trackLength + double.Parse(track[currentPiece]["lengthThisFar"].ToString()) + currentPos;

					speed = estimatedSpeed();


					if (isTurboOk())
					{
						Console.WriteLine("TURBO!");
						send(new Turbo("ULIULIULI"));
						turbo = null;
					}
					else
					{
						double nc = Math.Max(Math.Abs(curr) / 45, 1);
						nc = Math.Sin(nc * Math.PI/2);
						throttle = Math.Max(nc, 0.35);
						double pip = getPosInPiece(piece, currentPos);
						brake = false;
						bool sloping = (next * currentAngle < -100) || (curr * currentAngle < -100);
						bool hardTurn = (curr == 0 && next != 0) || (Math.Abs(curr - next) > 20) || (curr * next < -20) || sloping;
						bool slowdown = (currAbs >= 45 && nextAbs >= 45);
						bool speeding = muchSpeed(currentPiece) && (hardTurn || isNextBad());
						if (slowdown || speeding)
						{
							if (speeding)
								Console.Write("!");
							else
								Console.Write("*");
							throttle /= 2 * (muchSpeed(piece) ? 1.5 : 1) * (sloping ? 1.5 : 1);
							brake = true;
						}
						else if (muchSpeed(piece) && pip > 0.65)
						{
							throttle *= 0.5f;
							Console.Write("#");
						}

						/*else if (Math.Abs(currentAngle - curr) > 12)
						{
							throttle *= 0.75f;
							Console.Write("?");
						}*/

						send(new Throttle(throttle));
					}

					if (!crashed)
					{
						//if (lastTrackPosition > currentTrackPosition)
						//	Console.WriteLine("\nerror: " + lastTrackPosition + " > " + currentTrackPosition);
						int s = (int)(10 * speed);
						//Console.Write(" '" + (int)currentTrackPosition + "' ");
						Console.Write(" '" + s + "' ");
						//Console.Write(currentPiece + ":" + (int)currentPos + "   ");
					}
//						Console.Write(throttle);
					//Console.Write(".");
					break;
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
					break;
				case "gameInit":
					var vp = ((JToken)msg.data)["race"]["track"];
					track = (JArray)vp["pieces"];
					for (int a = 0; a < int.Parse(track.Count.ToString()); a++)
					{
						track[a]["lengthThisFar"] = trackLength;
						trackLength += getLength(a);

						Console.WriteLine("part[" + a + "] length: " + getLength(a) + "   / total: " + trackLength);
					}
					Console.WriteLine( "track length: " + trackLength);
					//Console.WriteLine("Race init: " + track  );
					send(new Ping());
					break;
				case "turboAvailable":
					turbo = (JToken) msg.data;
					break;
				case "gameEnd":
					Console.WriteLine("\nRace ended");
					send(new Ping());
					break;
				case "crash":
					speed = 0;
					crashed = true;
					Console.WriteLine("\ncrash :DD   @ " + currentPiece + ":" + (int)currentPos + "  angle: " + (int)currentPieceAngle + "     my angle: " + (int)currentAngle + (brake ? "      braking" : ""));
					//Console.WriteLine(msg.data);
					crashes++;
					send(new Ping());
					break;
				case "spawn":
					crashed = false;
					Console.WriteLine("\nspawn\n");
					send(new Ping());
					break;
				case "finish":
					Console.WriteLine("\nfinish with " + crashes + " crashes\n" + msg.data);
					send(new Ping());
					break;
				case "dnf":
					Console.WriteLine("disqualified");
					send(new Ping());
					break;
				case "lapFinished":
					Console.WriteLine("\nlap finished\n" + msg.data + "\n");
					send(new Ping());
					break;
				case "tournamentEnd":
					Console.WriteLine("Torment ended");
					send(new Ping());
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					send(new Ping());
					break;
				case "turboStart":
					if (((JToken)msg.data)["name"].ToString() == botName)
						turboOn = true;
					break;
				case "turboEnd":
					if (((JToken)msg.data)["name"].ToString() == botName)
						turboOn = false;
					break;

				default:
					Console.WriteLine( msg.msgType + " : " + msg.data);
					send(new Ping());
					break;
			}

			send(new Ping());
//			send(new Throttle(1.0));
		}
		Console.WriteLine(":::::::::::::::::: exit");
	}

	private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}
}

class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}

class Join: SendMsg {
	public string name;
	public string key;
	public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

class Turbo: SendMsg {
	public string value;

	public Turbo(string value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "turbo";
	}
}

class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}